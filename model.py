import random
import numpy as np
from PIL import Image
import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torch.autograd as autograd
from torch.nn import functional as F
import torchvision.models as models
import torchvision.transforms as transforms
from scipy import misc
import cv2
import judger_medical as judger
from utils import *
from evaluation import *




class Flatten(nn.Module):
    def forward(self, input):
        return input.view(input.size(0), -1)

class ImageNet(nn.Module):
    def __init__(self, model_name, num_classes, use_pretrain):
        super(ImageNet, self).__init__()
        self.model_name = model_name
        self.num_classes = num_classes

        if model_name == 'resnet101':
            original_model = models.resnet101(pretrained=use_pretrain)
        if model_name == 'resnet50':
            original_model = models.resnet50(pretrained=use_pretrain)
        if model_name == 'vgg19':
            original_model = models.vgg19(pretrained=use_pretrain)
        if model_name == 'vgg16':
            original_model = models.vgg16(pretrained=use_pretrain)
        if model_name == 'densenet201':
            original_model = models.densenet201(pretrained=use_pretrain)
        if model_name == 'densenet121':
            original_model = models.densenet121(pretrained=use_pretrain)  
        if model_name == 'alexnet':
            original_model = models.alexnet(pretrained=use_pretrain)  
        if model_name.startswith('vgg'):
            self.features = original_model.features
        if model_name.startswith('alexnet'):
            self.features = original_model.features
        if model_name.startswith('densenet'):
            self.features = original_model.features
        if model_name.startswith('resnet'):
            self.features = nn.Sequential(*list(original_model.children())[:-2])
        bs, c, w, h = self._get_conv_output([3, 224, 224])
        self.classifier = nn.Sequential(
            nn.AvgPool2d(w, 1),
            Flatten(),
            nn.Linear(c, self.num_classes),
        )
       
    def _get_conv_output(self, shape):
        input = Variable(torch.rand(1, *shape), volatile = True)
        output_feat = self.features(input)
        f_shape = output_feat.data.shape
        return f_shape

    def forward(self, x):
        fmap = self.features(x)
        output = self.classifier(fmap)
        return output

class CAM(object):
    def __init__(self, args):

        self.use_cuda = args.use_cuda

        self.attention_layer = args.attention_layer
        self.epochs = args.epochs
        self.lr = args.lr

        self.img_dir = args.img_dir
        self.sample_dir = args.sample_dir
        self.model_dir = args.model_dir
        self.exp_dir = args.exp_dir
        self.box_path = args.box_path
        
        self.log_interval = args.log_interval
        self.save_interval = args.save_interval

        self.label2idx, self.idx2label, \
        self.label2label, self.label2size = load_meta(args)
        self.use_pretrain = args.use_pretrain

        self.tr_hist = []
        self.val_hist = []
        self.features_blobs = []

        self.pretrain_model = args.pretrain_model

        self.bbox_df = pd.read_csv(self.box_path, sep=',')
        self.bbox_df.dropna(axis=1, how='all', inplace=True)

        self.image_net = ImageNet(
            args.pretrain_model, 
            len(self.label2idx), 
            self.use_pretrain)
        self.steps = 0

        self.opt = optim.Adam(self.image_net.parameters(), lr=self.lr)
        if args.use_cuda:  
            self.image_net.cuda()
        if args.load_model:
            self.load_image_net(args.imagenet_path)

        self.init_class_weight(args.weight_flag)
            
    def train_image_net(self, train_loader, valid_loader):
        for e in range(self.epochs):
            LOSSs = []
            PREDs = []
            GTs = []
            for i, x in enumerate(train_loader):
                self.steps += 1
                img = x['image']
                label = x['label']
                if self.use_cuda:  
                    img_v = Variable(img.cuda())  
                    lable_v = Variable(label.cuda())  
                else :
                    img_v = Variable(img)  
                    lable_v = Variable(label)  
                self.opt.zero_grad()   
                logit = self.image_net(img_v)
                pred_v = F.sigmoid(logit)
                loss = self.bce_loss(pred_v, lable_v)
                loss.backward()
                self.opt.step()    
                gt = label.numpy() 
                pred = pred_v.data.cpu().numpy()
                _precision, _recall, _f1score, _auroc = compute_score(gt, pred)
                _loss = loss.data.cpu().numpy()[0]
                LOSSs.append(_loss)
                PREDs += list(pred)
                GTs += list(gt)
                if self.steps % self.save_interval == 0:
                    print('======== save ========')
                    self.save_image_net(self.steps)
                if self.steps % self.log_interval == 0:
                    print('epoch : {} iters : {}'.format(e, self.steps)) 
                    print('loss : {:.3f} precision : {:.3f} recall : {:.3f}  f1score : {:.3f} auroc : {:.3f}'.format(
                        _loss, _precision, _recall, _f1score, _auroc))
            print('======== eval {} ========'.format(e))  
            pred = np.asarray(PREDs)
            gt = np.asarray(GTs)
            _precision, _recall, _f1score, _auroc = compute_score(gt, pred)
            _loss = np.mean(LOSSs)
            self.tr_hist.append([_loss, _precision, _recall, _f1score, _auroc])
            
            print('======== train ========')
            print('loss : {:.3f} precision : {:.3f} recall : {:.3f}  f1score : {:.3f} auroc : {:.3f}'.format(
                _loss, _precision, _recall, _f1score, _auroc))
            self.eval_image_net(valid_loader)

    def eval_image_net(self, valid_loader):  
        PREDs = []
        GTs = []
        LOSSs = []
        self.image_net.eval()
        for i, x in enumerate(valid_loader):  
            img = x['image']
            label = x['label']
            if self.use_cuda:  
                img_v = Variable(img.cuda())  
                lable_v = Variable(label.cuda())  
            else :
                img_v = Variable(img)  
                lable_v = Variable(label)  
            logit = self.image_net(img_v)
            pred_v = F.sigmoid(logit)
            _loss = self.bce_loss(pred_v, lable_v).data.cpu().numpy()[0]
            LOSSs.append(_loss)
            GTs += list(label.numpy() )
            PREDs += list(pred_v.data.cpu().numpy())

        pred = np.asarray(PREDs)
        gt = np.asarray(GTs)
        _loss = np.mean(LOSSs)        
        _precision, _recall, _f1score, _auroc = compute_score(gt, pred)

        self.val_hist.append([_loss, _precision, _recall, _f1score, _auroc])
        print('======== validation ========')
        print('loss : {:.3f} precision : {:.3f} recall : {:.3f}  f1score : {:.3f} auroc : {:.3f}'.format(
            _loss, _precision, _recall, _f1score, _auroc))
                
        self.image_net.train()

        np.save('{}/imagenet_{}_tr_best.npy'.format(
            self.exp_dir, self.pretrain_model), np.asarray(self.tr_hist))  
        np.save('{}/imagenet_{}_val_best.npy'.format(
            self.exp_dir, self.pretrain_model), np.asarray(self.val_hist))

    def save_image_net(self, iters):
        torch.save(self.image_net.state_dict(), '{}/imagenet_{}_{}_best.pkl'.format(
            self.model_dir, self.pretrain_model, iters))             
    
    def load_image_net(self, path):
        self.image_net.load_state_dict(torch.load(path, map_location=lambda storage, loc: storage))

    def preprocess(self, img):
        normalize = transforms.Normalize(
            mean=[0.485, 0.456, 0.406],
            std=[0.229, 0.224, 0.225]
        )
        preprocess = transforms.Compose([
            transforms.Resize((224,224)),
            transforms.ToTensor(),
            normalize
        ])
        img_tensor = preprocess(img)
        return img_tensor

    def init_class_weight(self, flag):
        
        if flag == 0 :
            class_weight = 0.5 * np.ones(N_CLASSES)    
            class_weight = torch.from_numpy(class_weight).type(torch.FloatTensor)
        else :
            class_weight = np.asarray(CLASS_WEIGHT)
            class_weight = torch.from_numpy(class_weight).type(torch.FloatTensor)                
            
        if self.use_cuda:
            class_weight = Variable(class_weight.cuda())
        else :
            class_weight = Variable(class_weight)

        # without weight
        if flag == 0:
            self.w_neg = class_weight
            self.w_pos = class_weight

        # with linear weight        
        if flag == 1:
            self.w_neg = class_weight
            self.w_pos = 1 - class_weight

        # with log weight        
        if flag == 2:    
            w_neg = 1 / (1 - class_weight)
            w_pos = 1 / class_weight 
            w_neg = torch.log(w_neg + 1 ) 
            w_pos = torch.log(w_pos + 1 ) 
            self.w_neg = w_neg / (w_pos + w_neg)
            self.w_pos = w_pos / (w_pos + w_neg)
            
        # with sqrt weight        
        if flag == 3:    
            w_neg = 1 / (1 - class_weight)
            w_pos = 1 / class_weight 
            w_neg = torch.sqrt(w_neg) 
            w_pos = torch.sqrt(w_pos) 
            self.w_neg = w_neg / (w_pos + w_neg)
            self.w_pos = w_pos / (w_pos + w_neg)
            
    def bce_loss(self, output, target):
        assert isinstance(output, Variable) and isinstance(target, Variable), \
        'output and target should be torch Variable'
        assert output.size() == target.size(), 'size of output and target are mismatch'
        target = target.float()
        return -(self.w_pos * target * torch.log(output + 1e-8) + \
            self.w_neg * (1.0 - target) * torch.log(1.0 - output + 1e-8)).sum(dim=1).mean()

    def visulize_bbox(self, img_list):
        self.image_net.eval()
        average_iou = []
        count = []
        for p in img_list:
            path = '{}{}'.format(self.img_dir, p)
            img = Image.open(path).convert('RGB')   
            img_df = self.bbox_df[self.bbox_df['Image Index']==p]                
            for row in img_df.values.tolist():
                _, label, x_gt, y_gt, w_gt, h_gt = row
                label = self.label2label[label]
                idx = self.label2idx[label]
                B = self.calculate_bbox(img, idx, self.attention_layer)
                cam_img, prob = self.calculate_cam(img, idx, self.attention_layer)                
                result = draw_heatmap(img, cam_img)                
                I = []
                gt = (x_gt, y_gt, w_gt, h_gt)                
                for x_pred, y_pred, w_pred, h_pred in B:
                    result = draw_bbox(result, x_pred, y_pred, w_pred, h_pred, 'blue')
                    pred = (x_pred, y_pred, w_pred, h_pred)
                    iou = compute_IoU(pred, gt)
                    I.append(iou)
                average_iou.append(np.max(I))
                count.append(len(I))
                result = draw_bbox(result, x_gt, y_gt, w_gt, h_gt, 'green')
                p = p.replace('.png', '')
                cv2.putText(result, "{}, IoU: {:.4f}, Prob: {:.4f}".format(label, np.max(I), prob), (10, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
                cv2.imwrite('{}/{}_{}.jpg'.format(self.sample_dir, p, label), result)
        self.image_net.train()
        print("Average IoU: {} Average # box: {}".format(np.mean(average_iou), np.mean(count)))

    def calculate_cam(self, img, idx, layer_idx):
        assert layer_idx == 1 or self.pretrain_model != 'densenet121' 
        if layer_idx == 1 :
            return self.calculate_cam_last_layer(img, idx)
        features_blobs = []
        finalconv_name = 'features'
        i = 0
        weight_list = []
        def hook_output(module, input, output):
            features_blobs.append(output.data.cpu().numpy())
        def hook_input(module, input):
            features_blobs.append(input[0].data.cpu().numpy())
        first = 0  
        for p in reversed(list(self.image_net.modules())):
            if isinstance(p, nn.Sequential) or isinstance(p, models.resnet.Bottleneck):
                for t in reversed(list(p.children())):
                    if isinstance(t, nn.Linear) or isinstance(t, nn.Conv2d):
                        p.register_forward_pre_hook(hook_input)
                        weight = t.cpu().weight.data.numpy()
                        while len(weight.shape) > 2:
                            weight = weight.mean(-1)
                        weight_list.append(weight)
                        i += 1
                    if i == layer_idx:
                        break
            if i == layer_idx:
                break
     
        img_tensor = self.preprocess(img)
        if self.use_cuda:
            img_v = Variable(img_tensor.unsqueeze(0).cuda())
        else:
            img_v = Variable(img_tensor.unsqueeze(0))
        logit = self.image_net(img_v)

        h_x = F.softmax(logit, dim=1).data.squeeze()
        size_upsample = (224, 224)
        cam_list = []
        features_blobs = features_blobs[::-1]

        for i in range(len(features_blobs)):
            feature_conv = features_blobs[i]
            bz, nc, h, w = feature_conv.shape
            feature_conv = feature_conv.reshape((nc, h*w))
            for j in reversed(range(1, i + 1)):
                feature_conv = weight_list[j].dot(feature_conv)
            cam = weight_list[0][idx].dot(feature_conv)
            cam = cam.reshape(h, w)
            cam_img = misc.imresize(cam, size_upsample)
            cam_list.append(cam_img)

        cam_img = np.product(cam_list, axis=0)
        cam_img = cam_img - np.min(cam_img)
        cam_img = cam_img / np.max(cam_img)
        cam_img = np.uint8(255 * cam_img)
        return cam_img, h_x[idx]
            
    def calculate_cam_last_layer(self, img, idx):
        self.features_blobs = []
        finalconv_name = 'features'
        def hook_feature(module, input, output):
            self.features_blobs.append(output.data.cpu().numpy())
        self.image_net._modules.get(finalconv_name).register_forward_hook(hook_feature)        
        img_tensor = self.preprocess(img)
        if self.use_cuda:
            img_v = Variable(img_tensor.unsqueeze(0).cuda())
        else:
            img_v = Variable(img_tensor.unsqueeze(0))
        logit = self.image_net(img_v)
        h_x = F.softmax(logit, dim=1).data.squeeze()
        params = list(self.image_net.parameters())
        weight_softmax = np.squeeze(params[-2].data.cpu().numpy())  
        feature_conv =  self.features_blobs[0]

        size_upsample = (224, 224)
        bz, nc, h, w = feature_conv.shape
        feature_conv = feature_conv.reshape((nc, h*w)) 
        cam = weight_softmax[idx].dot(feature_conv)
        cam = cam.reshape(h, w)
        cam = cam - np.min(cam)
        cam_img = cam / np.max(cam)
        cam_img = np.uint8(255 * cam_img)
        cam_img = misc.imresize(cam_img, size_upsample)
        return cam_img, h_x[idx]

    def calculate_bbox(self, img, idx, layer=1):
        label = self.idx2label[idx]
        w_avg, h_avg, s_avg  = self.label2size[label]
        cam_img, _ = self.calculate_cam(img, idx, layer)
        img = np.asarray(img)
        h, w, c = img.shape
        graymap = cv2.applyColorMap(cv2.resize(cam_img, (w, h)), cv2.COLORMAP_BONE)
        _graymap = cv2.cvtColor(graymap, cv2.COLOR_BGR2GRAY)
        h_mu = _graymap.mean()
        thres = int((h_mu + 255) / 2)
        B = []
        while True:
            ret, binary = cv2.threshold(_graymap, thres, 255, cv2.THRESH_BINARY)  
            _, contours, _ = cv2.findContours(binary,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)  
            S = []
            for cnt in contours:
                x, y, w, h = cv2.boundingRect(cnt)
                s = w * h
                S.append(s)                
                r = min(s / s_avg, s_avg / s)
                if r > R_THRES:
                    B.append([x, y, w, h])
            if len(B) != 0:
                break
            if np.mean(S) > s_avg:
                thres = int((thres + 255) / 2)
            else:
                thres = int((thres + h_mu) / 2)
        return B
    
    # for htc competition
    
    def inference(self, path):
        answer = []
        self.image_net.eval()  
        img = Image.open(path).convert('RGB')      
        features_blobs = []
        finalconv_name = 'features'
        def hook_feature(module, input, output):
            features_blobs.append(output.data.cpu().numpy())
        self.image_net._modules.get(finalconv_name).register_forward_hook(hook_feature)        
        img_tensor = self.preprocess(img)
        if self.use_cuda:
            img_v = Variable(img_tensor.unsqueeze(0).cuda())
        else:
            img_v = Variable(img_tensor.unsqueeze(0))
        logit = self.image_net(img_v)
        h_x = F.softmax(logit, dim=1).data.squeeze()
        probs, idxs = h_x.sort(0, True)   
        count = 0
        for idx in idxs[:5]:  
            box = {}   
            if self.idx2label[idx] in label2size.keys():
                count += 1
                box['class_name'] = self.idx2label[idx] 
                box['x'], box['y'], box['w'], box['h'] = self.calculate_bbox(img, idx)                 
                answer.append(box)
            if count >= 3 :
                break
        return answer

    def test(self):
        self.image_net.eval()  
        img_list = judger.get_file_names()
        f = judger.get_output_file_object()
        for img in img_list:
            answer = self.inference(p)
            f.write('%s %d\n' % (img, len(answer)))
            for box in answer:
                f.write('%s %f %f %f %f\n' % (box.class_name, box.x, box.y, box.w, box.h))
        score, err = judger.judge()
        if err is not None: 
            print(err)
