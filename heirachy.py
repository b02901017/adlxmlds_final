import random

import PIL
import numpy as np
import pandas as pd
from PIL import Image
from torch.utils.model_zoo import tqdm
from util import *
import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torch.autograd as autograd
from torch.nn import functional as F
import torchvision.models as models
import torchvision.transforms as transforms
from scipy import misc
import cv2
from tqdm import *
from random import choice
import os.path

CW = [0.1030949, 0.02475919, 0.11877453, 0.17743489, 0.05156975, 0.05646629,
      0.0127631, 0.04728862, 0.04162504, 0.02054049, 0.02244024, 0.01503746,
      0.0301908, 0.00202462]

normalize = transforms.Normalize([0.485, 0.456, 0.406],
                                 [0.229, 0.224, 0.225])

# filter no finding
# CW = [  0.22332348, 0.05363318, 0.25728859, 0.38435828, 0.1117100, 0.12231689,
#         0.02764737, 0.10243629, 0.09016789, 0.04449468, 0.0486099,  0.0325740,
#         0.06539925, 0.00438571]


class Flatten(nn.Module):
    def forward(self, input):
        return input.view(input.size(0), -1)


class ImageNet(nn.Module):
    def __init__(self, model_name, num_classes, use_pretrain):
        super(ImageNet, self).__init__()
        self.model_name = model_name
        self.num_classes = num_classes

        if model_name == 'resnet101':
            original_model = models.resnet101(pretrained=use_pretrain)
        if model_name == 'resnet50':
            original_model = models.resnet50(pretrained=use_pretrain)
        if model_name == 'vgg19':
            original_model = models.vgg19(pretrained=use_pretrain)
        if model_name == 'vgg16':
            original_model = models.vgg16(pretrained=use_pretrain)
        if model_name == 'densenet201':
            original_model = models.densenet201(pretrained=use_pretrain)
        if model_name == 'densenet121':
            original_model = models.densenet121(pretrained=use_pretrain)
        if model_name == 'alexnet':
            original_model = models.alexnet(pretrained=use_pretrain)
        if model_name.startswith('vgg'):
            self.features = original_model.features
        if model_name.startswith('alexnet'):
            self.features = original_model.features
        if model_name.startswith('densenet'):
            self.features = original_model.features
        if model_name.startswith('resnet'):
            self.features = nn.Sequential(*list(original_model.children())[:-2])
        bs, c, w, h = self._get_conv_output([3, 224, 224])
        self.classifier = nn.Sequential(
            nn.AvgPool2d(w, 1),
            Flatten(),
            nn.Linear(c, self.num_classes),
        )

    def _get_conv_output(self, shape):
        input = Variable(torch.rand(1, *shape), volatile=True)
        output_feat = self.features(input)
        f_shape = output_feat.data.shape
        return f_shape

    def forward(self, x):
        fmap = self.features(x)
        output = self.classifier(fmap)
        return output

class CAM(object):
    def __init__(self, train_loader, valid_loader, args):
        self.train_loader = train_loader
        self.valid_loader = valid_loader

        self.use_cuda = args.use_cuda
        self.epochs = args.epochs
        self.lr = args.lr
        self.attention_layer = args.attention_layer
        self.batch_size = args.batch_size

        self.img_dir = args.img_dir
        self.sample_dir = args.sample_dir
        self.model_dir = args.model_dir
        self.exp_dir = args.exp_dir
        self.box_path = args.box_path
        self.tag_path = args.tag_path
        self.train_data = load_txt(args.train_idx_path)

        self.save_interval = args.save_interval
        self.log_interval = args.log_interval
        self.eval_interval = args.eval_interval

        self.label2idx, self.idx2label, \
        self.label2label, self.label2size = load_meta(args)
        self.use_pretrain = args.use_pretrain

        self.tr_hist = []
        self.val_hist = []

        self.pretrain_model = args.pretrain_model

        self.bbox_df = pd.read_csv(self.box_path, sep=',')
        self.bbox_df.dropna(axis=1, how='all', inplace=True)

        ################
        # heirachy use #
        ################
        self.dtset_df = pd.read_csv(self.tag_path, sep=',')
        self.dtset_df.dropna(axis=1, how='all', inplace=True)
        self.dtset_df = self.dtset_df[self.dtset_df['Image Index'].isin(self.train_data)]
        self.dtset_df = self.dtset_df[self.dtset_df['Finding Labels']!='No Finding']

        self.bbox_average_size = 0.
        for key in self.label2size.keys():
            _, _, avg_size = self.label2size[key]
            self.bbox_average_size+=avg_size
        self.bbox_average_size = self.bbox_average_size/len(self.label2size)
        self.heirachy_net = ImageNet(
            args.pretrain_model,
            len(self.label2idx),
            self.use_pretrain)
            
        self.tranform =transforms.Compose([
                                 transforms.Resize(256),
                                 transforms.RandomCrop(224),
                                 transforms.RandomHorizontalFlip(),
                                 transforms.ToTensor(),
                                 normalize])

        self.image_net = ImageNet(
            args.pretrain_model,
            len(self.label2idx),
            self.use_pretrain)


        self.steps = 0

        self.params = list(self.image_net.parameters())

        self.opt = optim.Adam(self.image_net.parameters(), lr=self.lr)

        self.orignx_list = []
        self.origny_list = []
        self.img_list = []
        self.label_list = []
        
 
        self.opt_h = optim.Adam(self.heirachy_net.parameters(), lr=self.lr)
        # trainable_params = filter(lambda p: p.requires_grad, self.image_net.parameters())
        # self.opt = optim.Adam(trainable_params, lr=self.lr)
        if args.use_cuda:
            self.image_net.cuda()
            self.heirachy_net.cuda()
        if args.load_model:
            self.load_image_net(args.imagenet_path)

        self.init_class_weight(args.weight_flag)


    def train_image_net(self):
        for e in range(self.epochs):
            LOSSs = []
            PREDs = []
            GTs = []
            for i, x in enumerate(self.train_loader):
                self.steps += 1
                img = x['image']
                label = x['label']
                if self.use_cuda:
                    img_v = Variable(img.cuda())
                    lable_v = Variable(label.cuda())
                else:
                    img_v = Variable(img)
                    lable_v = Variable(label)

                self.opt.zero_grad()
                logit = self.image_net(img_v)
                pred_v = F.sigmoid(logit)
                loss = self.bce_loss(pred_v, lable_v)
                loss.backward()
                self.opt.step()
                gt = label.numpy()
                pred = pred_v.data.cpu().numpy()
                _precision, _recall, _f1score, _auroc = compute_score(gt, pred)
                _loss = loss.data.cpu().numpy()[0]
                LOSSs.append(_loss)
                PREDs += list(pred)
                GTs += list(gt)

                if self.steps % self.save_interval == 0:
                    print('======== save ========')
                    self.save_image_net(self.steps)
                if self.steps % self.log_interval == 0:
                    print('epoch : {} iters : {}'.format(e, self.steps))
                    print('loss : {:.3f} precision : {:.3f} recall : {:.3f}  f1score : {:.3f} auroc : {:.3f}'.format(
                        _loss, _precision, _recall, _f1score, _auroc))

            print('======== eval {} ========'.format(e))
            pred = np.asarray(PREDs)
            gt = np.asarray(GTs)
            _precision, _recall, _f1score, _auroc = compute_score(gt, pred)
            _loss = np.mean(LOSSs)
            self.tr_hist.append([_loss, _precision, _recall, _f1score, _auroc])

            print('======== train ========')
            print('loss : {:.3f} precision : {:.3f} recall : {:.3f}  f1score : {:.3f} auroc : {:.3f}'.format(
                _loss, _precision, _recall, _f1score, _auroc))
            self.eval_image_net()

            # print('======== visulize ========')
            # self.visulize_heapmap()

    def heirachy_dataprepare(self):
        self.origny_list = []
        self.orignx_list = []
        self.img_list = []
        self.label_list = []
        sample_train = self.dtset_df.sample(self.batch_size)
        sample_train = sample_train[['Image Index', 'Finding Labels']].values
        for row in sample_train:
            img, labels = row
            labels = labels.split('|')
            label = choice(labels)
            path = '{}{}'.format(self.img_dir, img)
            img = Image.open(path).convert('RGB')
            label = self.label2label[label]
            idx = self.label2idx[label]
            img, x, y = self.clipped_img(img, idx)
            img = PIL.Image.fromarray(np.uint8(img))
            img = self.tranform(img)
            self.orignx_list.append(x)
            self.origny_list.append(y)
            self.img_list.append(img)
            self.label_list.append(idx)

        self.label_list = np.asarray(self.label_list)
        return self.orignx_list, self.origny_list, self.img_list, self.label_list

    def train_heirachy_net(self):
        criterien = nn.CrossEntropyLoss()
        self.image_net.eval()
        hr_hist = []
        for epoch in range(20000):
            orignx_list, origny_list, img, label = self.heirachy_dataprepare()
            img = np.stack(img)
            hLOSSs = []
            hPREDs = []
            hGTs = []
            self.heirachy_net.zero_grad()
            self.steps += 1

            if self.use_cuda:
                img_v = Variable(torch.FloatTensor(img).cuda())
                lable_v = Variable(torch.LongTensor(label).cuda())
            else:
                img_v = Variable(img)
                lable_v = Variable(label)

            self.opt_h.zero_grad()
            logit = self.heirachy_net(img_v)
            pred_v = F.softmax(logit, dim=1)
            loss = criterien(pred_v, lable_v)
            loss.backward()
            self.opt_h.step()
            gt = lable_v.data.cpu()
            values, pred = torch.max(pred_v.data.cpu(), 1)
            score = pred.eq(gt).sum()
            _loss = loss.data.cpu().numpy()[0]
            hLOSSs.append(_loss)
            hPREDs += list(pred)
            hGTs += list(gt)
            hr_hist.append([loss, score/self.batch_size])
            if self.steps % self.save_interval == 0:
                print('======== save ========')
                self.save_net(self.steps)
            if self.steps % 1 == 0:
                print('epoch : {} iters : {}'.format(epoch, self.steps))
                print('loss : {:.3f}| accu: {:.3f}'.format(
                    _loss, score/self.batch_size))

            np.save('{}/imagenet_{}_tr_hr.npy'.format(
                self.exp_dir, self.pretrain_model), np.asarray(hr_hist))

        #self.image_net.train()


    def eval_image_net(self):
        PREDs = []
        GTs = []
        LOSSs = []
        self.image_net.eval()
        for i, x in enumerate(self.valid_loader):
            img = x['image']
            label = x['label']
            if self.use_cuda:
                img_v = Variable(img.cuda())
                lable_v = Variable(label.cuda())
            else:
                img_v = Variable(img)
                lable_v = Variable(label)
            logit = self.image_net(img_v)
            pred_v = F.sigmoid(logit)
            _loss = self.bce_loss(pred_v, lable_v).data.cpu().numpy()[0]
            LOSSs.append(_loss)
            GTs += list(label.numpy())
            PREDs += list(pred_v.data.cpu().numpy())

        pred = np.asarray(PREDs)
        gt = np.asarray(GTs)
        _loss = np.mean(LOSSs)
        _precision, _recall, _f1score, _auroc = compute_score(gt, pred)

        self.val_hist.append([_loss, _precision, _recall, _f1score, _auroc])
        print('======== validation ========')
        print('loss : {:.3f} precision : {:.3f} recall : {:.3f}  f1score : {:.3f} auroc : {:.3f}'.format(
            _loss, _precision, _recall, _f1score, _auroc))

        self.image_net.train()

        np.save('{}/imagenet_{}_tr_log.npy'.format(
            self.exp_dir, self.pretrain_model), np.asarray(self.tr_hist))
        np.save('{}/imagenet_{}_val_log.npy'.format(
            self.exp_dir, self.pretrain_model), np.asarray(self.val_hist))

    def save_net(self, iters):
        torch.save(self.heirachy_net.state_dict(), '{}/heirachy_net_{}_{}_log.pkl'.format(
            self.model_dir, self.pretrain_model, iters))

    def load_image_net(self, path):
        self.image_net.load_state_dict(torch.load(path, map_location=lambda storage, loc: storage))
        self.heirachy_net.load_state_dict(torch.load(path, map_location=lambda storage, loc: storage))

    def visulize_heapmap(self, img_list):
        self.image_net.eval()
        for p in img_list:
            path = '{}{}'.format(self.img_dir, p)
            img = Image.open(path).convert('RGB')
            img_df = self.bbox_df[self.bbox_df['Image Index'] == p]
            for row in img_df.values.tolist():
                _, label, x_gt, y_gt, w_gt, h_gt = row
                label = self.label2label[label]
                idx = self.label2idx[label]
                result = np.asarray(img)
                heatmap = self.draw_heapmap(img, idx)
                result = heatmap * 0.3 + result * 0.5
                result = self.draw_bbox(result, x_gt, y_gt, w_gt, h_gt, 0)
                p = p.replace('.png', '')
                cv2.imwrite('{}/heapmap/{}_{}.jpg'.format(self.sample_dir, p, label), result)
        self.image_net.train()

    def visulize_bbox(self, img_list):
        self.image_net.eval()
        for p in img_list:
            path = '{}{}'.format(self.img_dir, p)
            img = Image.open(path).convert('RGB')
            img_df = self.bbox_df[self.bbox_df['Image Index'] == p]
            for row in img_df.values.tolist():
                _, label, x_gt, y_gt, w_gt, h_gt = row
                label = self.label2label[label]
                idx = self.label2idx[label]
                result = np.asarray(img)
                heatmap = self.draw_heapmap(img, idx)
                x_pred, y_pred, w_pred, h_pred = self.calculate_bbox(img, idx)
                result = heatmap * 0.3 + result * 0.5
                result = self.draw_bbox(result, x_pred, y_pred, w_pred, h_pred, 1)
                result = self.draw_bbox(result, x_gt, y_gt, w_gt, h_gt, 0)
                p = p.replace('.png', '')
                cv2.imwrite('{}/bbox/{}_{}.jpg'.format(self.sample_dir, p, label), result)
        self.image_net.train()

    def draw_heapmap(self, img, idx):
        if self.attention_layer == 1:
            cam_img = self.calculate_cam(img, idx)
        else:
            cam_img = self.calculate_cam_multilayer(img, idx, self.attention_layer)
        img = np.asarray(img)
        h, w, c = img.shape
        heatmap = cv2.applyColorMap(cv2.resize(cam_img, (w, h)), cv2.COLORMAP_JET)
        return heatmap

    def draw_bbox(self, img, x, y, w, h, flag):
        if (flag == 0):
            img = cv2.rectangle(img, (int(x), int(y)), (int(x + w), int(y + h)), (0, 255, 0), 2)
        if (flag == 1):
            img = cv2.rectangle(img, (int(x), int(y)), (int(x + w), int(y + h)), (255, 0, 0), 2)
        return img

    def calculate_cam(self, img, idx):
        self.features_blobs = []
        finalconv_name = 'features'

        def hook_feature(module, input, output):
            self.features_blobs.append(output.data.cpu().numpy())
        self.image_net._modules.get(finalconv_name).register_forward_hook(hook_feature)
        img_tensor = self.preprocess(img)
        if self.use_cuda:
            img_v = Variable(img_tensor.unsqueeze(0).cuda(), volatile=True)
        else:
            img_v = Variable(img_tensor.unsqueeze(0), volatile=True)
        logit = self.image_net(img_v)
        #h_x = F.softmax(logit, dim=1).data.squeeze()
        # probs, idx = h_x.sort(0, True)
        weight_softmax = np.squeeze(self.params[-2].data.cpu().numpy())
        feature_conv = self.features_blobs[0]
        
        size_upsample = (224, 224)
        bz, nc, h, w = feature_conv.shape
        feature_conv = feature_conv.reshape((nc, h * w))
        cam = weight_softmax[idx].dot(feature_conv)
        cam = cam.reshape(h, w)
        cam = cam - np.min(cam)
        cam_img = cam / np.max(cam)
        cam_img = np.uint8(255 * cam_img)
        cam_img = misc.imresize(cam_img, size_upsample)
        return cam_img

    def calculate_cam_multilayer(self, img, idx, layer_num):
        # assert layer_num <= 3
        features_blobs = []
        finalconv_name = 'features'
        i = 0
        weight_list = []

        def hook_output(module, input, output):
            features_blobs.append(output.data.cpu().numpy())

        def hook_input(module, input):
            features_blobs.append(input[0].data.cpu().numpy())

        # self.image_net.register_forward_pre_hook(hook_feature)
        first = 0
        for p in reversed(list(self.image_net.modules())):
            if isinstance(p, nn.Sequential) or isinstance(p, models.resnet.Bottleneck):
                for t in reversed(list(p.children())):
                    if isinstance(t, nn.Linear) or isinstance(t, nn.Conv2d):
                        p.register_forward_pre_hook(hook_input)
                        weight = t.cpu().weight.data.numpy()
                        while len(weight.shape) > 2:
                            weight = weight.mean(-1)
                        weight_list.append(weight)
                        i += 1
                    if i == layer_num:
                        break
            # elif isinstance(p, models.resnet.Bottleneck):
            #     for t in reversed(list(p.children())):
            #         if isinstance(t, nn.Conv2d):
            #             t.register_forward_pre_hook(hook_input)
            #             weight = t.cpu().weight.data.numpy()
            #             while len(weight.shape) > 2:
            #                 weight = weight.mean(-1)

            #             weight_list.append(weight)
            #             i += 1
            #         if i == layer_num:
            #             break
            if i == layer_num:
                break

            # if isinstance(p, nn.Linear):
            #     if i != 0:
            #         p.register_forward_hook(hook_feature)
            #     if i == layer_num:
            #         break
            #     weight = p.cpu().weight.data.numpy()
            #     while len(weight.shape) > 2:
            #         weight = weight.mean(-1)
            #     weight_list.append(weight)
            #     i += 1

        img_tensor = self.preprocess(img)
        if self.use_cuda:
            img_v = Variable(img_tensor.unsqueeze(0).cuda())
        else:
            img_v = Variable(img_tensor.unsqueeze(0))
        logit = self.image_net(img_v)

        h_x = F.softmax(logit, dim=1).data.squeeze()
        # probs, idx = h_x.sort(0, True)
        # params = list(self.image_net.parameters())
        # weight_softmax = np.squeeze(params[-2].data.cpu().numpy())
        # features_blobs = reversed(features_blobs)
        size_upsample = (224, 224)
        cam_list = []
        features_blobs = features_blobs[::-1]
        # for i in range(layer_num):
        #     print(features_blobs[i].shape)
        #     print(weight_list[i].shape)
        # print(list(self.image_net.modules())[-4:])
        for i in range(len(features_blobs)):
            feature_conv = features_blobs[i]
            # print(features_blobs[i].shape)
            bz, nc, h, w = feature_conv.shape
            feature_conv = feature_conv.reshape((nc, h * w))
            for j in reversed(range(1, i + 1)):
                feature_conv = weight_list[j].dot(feature_conv)
            cam = weight_list[0][idx].dot(feature_conv)
            cam = cam.reshape(h, w)
            # cam = cam - np.min(cam)
            # cam_img = cam / np.max(cam)
            # cam_img = np.uint8(255 * cam_img)
            cam_img = misc.imresize(cam, size_upsample)
            cam_list.append(cam_img)

        cam_img = np.product(cam_list, axis=0)

        cam_img = cam_img - np.min(cam_img)
        cam_img = cam_img / np.max(cam_img)
        cam_img = np.uint8(255 * cam_img)
        return cam_img


    def calculate_bbox(self, img, idx):
        label = self.idx2label[idx]
        if label in self.label2size:
            w_avg, h_avg, s_avg = self.label2size[label]
        else:
            s_avg = self.bbox_average_size
        cam_img = self.calculate_cam(img, idx)
        img = np.asarray(img)
        h, w, c = img.shape
        graymap = cv2.applyColorMap(cv2.resize(cam_img, (w, h)), cv2.COLORMAP_BONE)
        _graymap = cv2.cvtColor(graymap, cv2.COLOR_BGR2GRAY)
        thres = int((_graymap.mean() + 255) / 2)

        while True:
            ret, binary = cv2.threshold(_graymap, thres, 255, cv2.THRESH_BINARY)
            _, contours, _ = cv2.findContours(binary, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            areas = [cv2.contourArea(c) for c in contours]
            area_idxs = np.argsort(areas)
            area_idxs = area_idxs[::-1]
            r_best = MIN
            for a in area_idxs:
                cnt = contours[a]
                x, y, w, h = cv2.boundingRect(cnt)
                s = w * h
                r = min(s / s_avg, s_avg / s)
                if r > r_best:
                    r_best = r
                    x_best, y_best, w_best, h_best = x, y, w, h
                    s_best = s
            if r_best < R_THRES:
                if s_best > s_avg:
                    thres = int((thres + 255) / 2)
                else:
                    thres = int((thres + _graymap.mean()) / 2)
            else:
                break
        return x_best, y_best, w_best, h_best

    def clipped_img(self, img, idx):
        x, y, w, h = self.calculate_bbox(img, idx)
        img = np.asarray(img)
        O_x = x+w/2-128
        O_y = y+h/2-128
        if O_x<0:
            x = 0
        elif O_x>0 and O_x+256<1024:
            x = O_x
        else:
            x = 1024-256
        if O_y<0:
            y = 0
        elif O_y >0 and O_y+256<1024:
            y = O_y
        else:
            y = 1024-256
        try :
            img = img[int(y): int(y + 256), int(x): int(x + 256), :]
        except :
            print(img.shape)
        return img, int(x), int(y)

    def preprocess(self, img):
        normalize = transforms.Normalize(
            mean=[0.485, 0.456, 0.406],
            std=[0.229, 0.224, 0.225]
        )
        preprocess = transforms.Compose([
            transforms.RandomCrop(224),
            transforms.ToTensor(),
            normalize
        ])
        img_tensor = preprocess(img)
        return img_tensor

    def init_class_weight(self, flag):

        if flag == 0:
            class_weight = 0.5 * np.ones(N_CLASSES)
            class_weight = torch.from_numpy(class_weight).type(torch.FloatTensor)
        else:
            class_weight = np.asarray(CW)
            class_weight = torch.from_numpy(class_weight).type(torch.FloatTensor)

        if self.use_cuda:
            class_weight = Variable(class_weight.cuda())

        else:
            class_weight = Variable(class_weight)

        # without weight
        if flag == 0:
            self.w_neg = class_weight
            self.w_pos = class_weight

        # with linear weight
        if flag == 1:
            self.w_neg = class_weight
            self.w_pos = 1 - class_weight

        # with log weight
        if flag == 2:
            w_neg = 1 / (1 - class_weight)
            w_pos = 1 / class_weight
            w_neg = torch.log(w_neg + 1)
            w_pos = torch.log(w_pos + 1)
            self.w_neg = w_neg / (w_pos + w_neg)
            self.w_pos = w_pos / (w_pos + w_neg)

        # with sqrt weight
        if flag == 3:
            w_neg = 1 / (1 - class_weight)
            w_pos = 1 / class_weight
            w_neg = torch.sqrt(w_neg)
            w_pos = torch.sqrt(w_pos)
            self.w_neg = w_neg / (w_pos + w_neg)
            self.w_pos = w_pos / (w_pos + w_neg)


    def bce_loss(self, output, target):
        assert isinstance(output, Variable) and isinstance(target, Variable), \
            'output and target should be torch Variable'
        assert output.size() == target.size(), 'size of output and target are mismatch'
        target = target.float()
        return -(self.w_pos * target * torch.log(output + 1e-8) + self.w_neg * (1.0 - target) * torch.log(1.0 - output + 1e-8)).sum(dim=1).mean()


    def train(self):
        pass


    def test(self, output_dir):
        pass


    def save(self, iters):
        pass
