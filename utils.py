# -*- coding: utf-8 -
import numpy as np 
import pandas as pd
import pickle
from torch.utils.data import Dataset, DataLoader
from scipy import misc
import os
from PIL import Image
from random import random, shuffle
import cv2
from constants import *


class ChestXDataset(Dataset):
	def __init__(self, labels, imgs, img_dir, transform):
		self.labels = labels
		self.imgs = imgs
		self.img_dir = img_dir
		self.transform = transform

	def __len__(self):
		return len(self.labels)

	def __getitem__(self, idx):
		img_path = os.path.join(self.img_dir, self.imgs[idx])
		image = Image.open(img_path).convert('RGB')
		if self.transform is not None:
			img = self.transform(image)
		label = self.labels[idx]
		sample = {'image': img, 'label': label}
		return sample

def dump_pkl(file, path):
	pickle.dump(file, open( path, 'wb'))	
	return 

def load_pkl(path):
	return pickle.load(open( path, 'rb'))	

def load_txt(path):
	return np.genfromtxt(path, dtype='str')

def load_train_data(args):
	fields = ['Image Index', 'Finding Labels', 'Patient Age', 'Patient Gender', 'View Position']
	d = pd.read_csv(args.tag_path, usecols=fields)
	labels = d['Finding Labels'].tolist()
	imgs = d['Image Index'].tolist()
	
	if args.prepro:
		label2idx, idx2label = build_dict(labels)	
		dump_pkl(label2label, '{}label2label.pkl'.format(args.meta_dir))
		dump_pkl(label2idx, '{}label2idx.pkl'.format(args.meta_dir))
		dump_pkl(idx2label, '{}idx2label.pkl'.format(args.meta_dir))
		dump_pkl(label2size, '{}label2size.pkl'.format(args.meta_dir))
	label2idx, idx2label, label2label, label2size = load_meta(args)
	labels = list(map(lambda x : x.split('|'), labels))
	labels = list(map(lambda x : one_hot(x, label2idx), labels))
	return labels, imgs

def load_meta(args):
	label2idx = load_pkl('{}label2idx.pkl'.format(args.meta_dir))
	idx2label = load_pkl('{}idx2label.pkl'.format(args.meta_dir))
	label2label = load_pkl('{}label2label.pkl'.format(args.meta_dir))
	label2size = load_pkl('{}label2size.pkl'.format(args.meta_dir))
	return label2idx, idx2label, label2label, label2size
	
def build_dict(arr):
    arr = '|'.join(arr)
    arr = arr.split('|')
    arr = set(arr)
    arr.remove(NOFINDING)
    label2idx = {}
    idx2label = {}
    for i, x in enumerate(arr):
        label2idx[x] = i
        idx2label[i] = x
    return label2idx, idx2label

def one_hot(x, dic):
    x_ = np.zeros(len(dic))
    if NOFINDING in x:
        return x_
    else :
    	for t in x:
    		x_[dic[t]] = 1.0
    return x_

def shuffle_arrs( *arrs):
    r = random()
    result = []
    for arr in arrs:       
        shuffle(arr, lambda : r)   
        result.append(arr)
    return tuple(result)

def draw_heatmap(img, cam_img):
	img = np.asarray(img)
	h, w, c = img.shape
	_img = cv2.applyColorMap(cv2.resize(cam_img, (w, h)), cv2.COLORMAP_JET)
	_img = _img * 0.3 + img * 0.5
	return _img

def draw_bbox(img, x, y, w, h, color):
	if(color == 'green') :
		_img = cv2.rectangle(img, (int(x),int(y)),(int(x+w),int(y+h)),(0,255,0),2)
	if(color == 'blue') :
		_img = cv2.rectangle(img, (int(x),int(y)),(int(x+w),int(y+h)),(255,0,0),2)
	if(color == 'red') :
		_img = cv2.rectangle(img, (int(x),int(y)),(int(x+w),int(y+h)),(0, 0,255),2)
	return _img
