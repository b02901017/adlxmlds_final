# -*- coding: utf-8 -
import torch
from model import *
from sklearn.metrics import roc_auc_score
from torch import nn, optim
from tqdm import tqdm
from util import *
from arguments import *
import pickle
import os
import io
import requests
from PIL import Image
from torchvision import models, transforms
from torch.autograd import Variable
from torch.nn import functional as F
import numpy as np
import cv2
import torch
import torch.nn as nn
import numpy as np
import torch.optim as optim
from torch.autograd import Variable

np.random.seed(7)
torch.manual_seed(7)


# (1, 0) => target labels 0+2
# (0, 1) => target labels 1
# (1, 1) => target labels 3
def binarize(pred):
    #print(pred)
    pred[pred > 0.] = 1
    pred[pred < 0.] = 0
    #print(pred)

    return pred

def compute_precision(gt, pred):
    PRECISIONS = []
    #pred = binarize(pred, 0.5)
    for i in range(14):
        t = gt[:, i]
        p = pred[:, i]
        t = t[np.where(p == 1)]
        p = p[np.where(p == 1)]
        val = np.mean(p == t)
        if not np.isnan(val):
            PRECISIONS.append(val)
    if not PRECISIONS:
        return 0
    else:
        return np.array(PRECISIONS).mean()

def compute_recall(gt, pred):
    RECALLs = []
    #pred = binarize(pred, 0.5)
    for i in range(14):
        #print(gt)
        t = gt[:, i]
        p = pred[:, i]
        t = t[np.where(t == 1)]
        p = p[np.where(t == 1)]
        val = np.mean(p == t)
        if not np.isnan(val):
            RECALLs.append(val)
    if not RECALLs:
        return 0
    else:
        return np.array(RECALLs).mean()

def compute_auroc(gt, pred):
    AUROCs = []
    for i in range(14):
        try:
            AUROCs.append(roc_auc_score(gt[:, i], pred[:, i]))
        except:
            pass
    return np.array(AUROCs).mean()

def postpro(y_pred):
    #print("origin", y_pred[:3])
    y_pred[y_pred<0] = 0
    '''row_mean = np.mean(y_pred, axis=1).reshape(-1, 1)
    y_pred = y_pred-np.repeat(row_mean, 14, axis=1)
    y_pred[y_pred < threshold] = 0'''
    for row in y_pred:
        #print(row)
        while len(row[row!=0.])>3:
            th = np.mean(row[row!=0.])
            row[row<th] = 0
            maxvalue = max(row)
            for b in row:
                if b!=0:
                    if b/maxvalue <0.5:
                        row[row==b]=0
        #print("-\n", row)
    y_pred[y_pred!=0] = 1
    #print("after", y_pred[:3])
    return y_pred

class _classifier(nn.Module):
    def __init__(self, nlabel):
        super(_classifier, self).__init__()
        self.main = nn.Sequential(
            nn.Linear(3 * 224 * 224, 1000),
            nn.ReLU(),
            nn.Linear(1000, nlabel),
        )

    def forward(self, input):
        return self.main(input)

class DenseNet121(nn.Module):
    """Model modified.
    The architecture of our model is the same as standard DenseNet121
    except the classifier layer which has an additional sigmoid function.
    """
    def __init__(self, out_size):
        super(DenseNet121, self).__init__()
        self.densenet121 = models.densenet121(pretrained=False)
        num_ftrs = self.densenet121.classifier.in_features
        self.densenet121.classifier = nn.Sequential(
            nn.Linear(num_ftrs, out_size),
            nn.Sigmoid()
        )

    def forward(self, x):
        x = self.densenet121(x)
        return x

class ResNet50(nn.Module):
    """Model modified.
    The architecture of our model is the same as standard DenseNet121
    except the classifier layer which has an additional sigmoid function.
    """
    def __init__(self, out_size):
        super(ResNet50, self).__init__()

        # Loading ResNet arch from PyTorch
        original_model = models.resnet50(pretrained=False)

        # Everything except the last linear layer
        self.features = nn.Sequential(*list(original_model.children())[:-1])

        # Get number of features of last layer
        num_feats = original_model.fc.in_features

        # Plug our classifier
        self.classifier = nn.Sequential(
            nn.Linear(num_feats, out_size)
        )

        # Init of last layer
        for m in self.classifier:
            nn.init.kaiming_normal(m.weight)

        # Freeze all weights except the last classifier layer
        # for p in self.features.parameters():
        #     p.requires_grad = False

    def forward(self, x):
        f = self.features(x)
        f = f.view(f.size(0), -1)
        y = self.classifier(f)
        return y

def train(args):
    best_acc = 0
    labels, imgs, label2idx, idx2label = load_train_data(args)
    traindata = TrainDataset(labels, imgs, args.img_dir)
    data_loader = DataLoader(traindata, batch_size=args.batch_size, shuffle=True, num_workers=4)

    # networks such as googlenet, resnet, densenet already use global average pooling at the end, so CAM could be used directly.
    #net = models.densenet121(pretrained=True)
    #finalconv_name = 'layer4'
    #net.fc = nn.Linear(512, len(label2idx))

    #print(net)
    #net.train()

    nlabel = 14 #len(labels[0])  # => 3

    #classifier = _classifier(nlabel)
    classifier = models.resnet152(pretrained=False)
    classifier.fc = nn.Linear(2048, nlabel)
    #print(classifier)
    optimizer = optim.Adam(classifier.parameters(), lr=args.lr)
    criterion = nn.MultiLabelSoftMarginLoss()

    epochs = 15
    best_acc = 0.65
    for epoch in range(epochs):
        losses = []
        print('\nEpoch: %d' % epoch)
        #train_loss = 0
        #correct = 0
        #total = 0
        for batch_idx, data in tqdm(enumerate(data_loader), leave=False):
            classifier.zero_grad()
            inputs, targets = data['image'], data['label']
            inputs, targets = Variable(inputs), Variable(targets.type(torch.FloatTensor))

            output = classifier.forward(inputs)
            #output = F.sigmoid(output)
            #print("infor", output[:1], torch.mean(output, 1)[:1])#, targets[:1])
            loss = criterion(output, targets)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            losses.append(loss.data.mean())

            auroc = compute_auroc(targets.data.numpy(), output.data.numpy())

            if batch_idx%10==0:
                pred = binarize(output.data.numpy())
                predicted = torch.FloatTensor(pred)
                m = torch.eq(predicted, targets.data).type(torch.FloatTensor)
                B = torch.mean(m, 1)
                correct = (inputs.size(0)-B[B!=1.].size(0))#sum(predicted.eq(targets.data))
                p = compute_precision(targets.data.numpy(), predicted.numpy())
                r = compute_recall(targets.data.numpy(), predicted.numpy())
                f1 = 2*p*r/(p+r) if (p!=0 and r!=0) else 0
                print('[%d/%d] Loss: %.3f | acc_num: %d/%d | F1: %.3f | AUROC: %.2f' % (epoch, batch_idx, np.mean(losses), correct, args.batch_size, f1, 100* auroc))
            else:
                print('[%d/%d] Loss: %.3f | AUROC: %.2f%%' % (epoch, batch_idx, np.mean(losses), 100 * auroc))
            if auroc >best_acc:
                print('Saving..')
                state = {
                    'net': classifier,
                    'acc': auroc,
                    'epoch': epoch,
                }
                if not os.path.isdir('zoo'):
                    os.mkdir('zoo')
                torch.save(state, './zoo/ckpt.t7')
                best_acc = auroc

if __name__ == '__main__':
    if not os.path.exists('meta/'):
        os.makedirs('meta/')
    if not os.path.exists('model/'):
        os.makedirs('model/')
    if not os.path.exists('sample/'):
        os.makedirs('sample/')
    if not os.path.exists('output/'):
        os.makedirs('output/')
    args = parse()
    train(args)
    if args.test:
        pass