# -*- coding: utf-8 -
from model import *
from utils import * 
from arguments import *
import os
import torchvision.transforms as transforms


def train(args):
    labels, imgs = load_train_data(args)
    if args.shuffle:
        labels, imgs = shuffle_arrs(labels, imgs)

    ach = int(args.valid_ratio * len(labels))
    labels_val = labels[:ach]
    imgs_val = imgs[:ach]
    labels_tr = labels[ach:]
    imgs_tr = imgs[ach:]

    normalize = transforms.Normalize([0.485, 0.456, 0.406],
                                     [0.229, 0.224, 0.225])

    traindata = ChestXDataset(labels_tr, imgs_tr, args.img_dir, 
                                transform=transforms.Compose([
                                    transforms.Resize(256),
                                    transforms.RandomCrop(224),
                                    transforms.RandomHorizontalFlip(),
                                    transforms.ToTensor(),
                                    normalize]))

    validdata = ChestXDataset(labels_val, imgs_val, args.img_dir, 
                                transform=transforms.Compose([
                                    transforms.Resize(224),
                                    transforms.ToTensor(),
                                    normalize]))

    train_loader = DataLoader(dataset = traindata, 
                                batch_size = args.batch_size, 
                                shuffle = True, 
                                num_workers = 2,
                                pin_memory = True)

    valid_loader = DataLoader(dataset = validdata, 
                                batch_size = args.batch_size // 2, 
                                shuffle = True, 
                                num_workers = 2,
                                pin_memory = True)

    cam = CAM(args) 
    cam.train_image_net(train_loader, valid_loader)

def draw(args):
    labels, imgs = load_train_data(args)
    valid_data = load_txt(args.valid_idx_path)
    test_data = load_txt(args.test_idx_path)
    cam = CAM(args) 
    if args.draw:
        cam.visulize_bbox(test_data)

def test(args):
    test_data = load_txt(args.test_idx_path)    
    cam = CAM(args) 
    cam.test()
    
if __name__ == '__main__':
    if not os.path.exists('meta/'):
        os.makedirs('meta/')
    if not os.path.exists('model/'):
        os.makedirs('model/')
    if not os.path.exists('sample/'):
        os.makedirs('sample/')
    if not os.path.exists('exp/'):
        os.makedirs('exp/')
    args = parse()
    print_args(args)
    if args.train:
        train(args)
    if args.draw:
        draw(args)
    if args.test:
        test(args)
        