# -*- coding: utf-8 -
import torch
from model import *
from torch import nn, optim
from util import * 
from arguments import *
import pickle
import os
import io
import requests
from PIL import Image
from torchvision import models, transforms
from torch.autograd import Variable
from torch.nn import functional as F
import numpy as np
import cv2

np.random.seed(0)
torch.manual_seed(0)

def returnCAM(feature_conv, weight_softmax, class_idx):
    # generate the class activation maps upsample to 256x256
    size_upsample = (256, 256)
    bz, nc, h, w = feature_conv.shape
    output_cam = []
    for idx in class_idx:
        cam = weight_softmax[class_idx].dot(feature_conv.reshape((nc, h * w)))
        cam = cam.reshape(h, w)
        cam = cam - np.min(cam)
        cam_img = cam / np.max(cam)
        cam_img = np.uint8(255 * cam_img)
        output_cam.append(cv2.resize(cam_img, size_upsample))
    return output_cam

def propro(y_pred):

    threshold = 0
    y_pred[y_pred<threshold] = 0
    row_mean = np.mean(y_pred, axis=1).reshape(-1, 1)
    y_pred = y_pred-np.repeat(row_mean*2, 14, axis=1)
    y_pred[y_pred < threshold] = 0
    for row in y_pred:
        #print(row)
        while len(row[row!=0.])>3:
            th = np.mean(row[row!=0.])
            row[row<th] = 0
        #print("-\n", row)
    y_pred[y_pred!=0] = 1
    #print("after", y_pred[:3])
    return y_pred


def train(args):
    labels, imgs, label2idx, idx2label = load_train_data(args)
    traindata = TrainDataset(labels, imgs, args.img_dir, 
                            transform=transforms.Compose([
                            transforms.Scale(256),
                            transforms.RandomCrop(224),
                            transforms.RandomHorizontalFlip(),
                            transforms.ToTensor(),
                            normalize]))
    data_loader = DataLoader(traindata, batch_size=args.batch_size, shuffle=True, num_workers=4)

    # networks such as googlenet, resnet, densenet already use global average pooling at the end, so CAM could be used directly.
    net = models.resnet50(pretrained=False)
    finalconv_name = 'layer4'
    net.fc = nn.Linear(2048, len(label2idx))

    #print(net)
    net.train()


    optimizer = optim.Adam(net.parameters(), lr=args.lr)#, momentum=0.9, weight_decay=5e-4)
    start_epoch = 0
    for epoch in range(start_epoch, start_epoch + 200):
        print('\nEpoch: %d' % epoch)
        train_loss = 0
        correct = 0
        total = 0
        for batch_idx, data in enumerate(data_loader):
            inputs, targets = data['image'], data['label']
            optimizer.zero_grad()
            inputs, targets = Variable(inputs), Variable(targets.type(torch.FloatTensor))
            outputs = net(inputs)
            #print(outputs)
            #sys.exit(1)
            predicted = propro(outputs.data.numpy())
            predicted = torch.from_numpy(predicted)

            loss = 0.5*torch.mean((outputs - targets)**2)
            loss.backward()
            optimizer.step()
            train_loss += loss.data[0]

            total += targets.size(0)
            m = torch.mean((predicted-targets.data), 1)
            #print(m[m!=0.].size(0))
            correct += (inputs.size(0)-m[m!=0.].size(0))

            print(batch_idx, len(data_loader), 'Loss: %.3f | Acc: %.3f%% (%d/%d)'
                         % (train_loss / (batch_idx + 1), 100. * correct / total, correct, total))
            if batch_idx%10==0:
                #############eval to mack heatmap
                net.eval()
                # hook the feature extractor
                features_blobs = []

                def hook_feature(module, input, output):
                    # print(output.data.cpu().numpy().shape)
                    features_blobs.append(output.data.cpu().numpy())

                net._modules.get(finalconv_name).register_forward_hook(hook_feature)
                # print(net._modules.get(finalconv_name))
                # get the softmax weight
                params = list(net.parameters())
                weight_softmax = np.squeeze(params[-2].data.numpy())

                normalize = transforms.Normalize(
                    mean=[0.485, 0.456, 0.406],
                    std=[0.229, 0.224, 0.225]
                )
                preprocess = transforms.Compose([
                    transforms.Scale((224, 224)),
                    transforms.ToTensor(),
                    normalize
                ])
                print("img id: ", imgs[0])
                # response = requests.get(IMG_URL)
                img_pil = Image.open('/tmp2/d04944017/ADLxMLDS2017/htc_project/images/' + imgs[0])

                img_tensor = preprocess(img_pil)
                img_variable = Variable(img_tensor.unsqueeze(0)).repeat(1, 3, 1, 1)
                # print(img_variable.size())
                # print(net)
                logit = net(img_variable)
                print("raw :\n", logit.data.numpy())
                logit = propro(logit.data.numpy())
                answer_list = []
                a = logit[0].tolist()
                print(a)
                i = 0
                for x in a:
                    if x ==1.:
                        answer_list.append(idx2label[i])
                        i+=1
                    else:
                        i+=1
                        continue
                answer = '|'.join(answer_list)

                classes = idx2label  # {int(key): value for (key, value) in requests.get(LABELS_URL).json().items()}

                h_x = F.softmax(Variable(torch.from_numpy(logit[0]))).data.squeeze()
                probs, idx = h_x.sort(0, True)

                # output the prediction
                print("| epoch %d/iter %d   |"%(epoch, batch_idx))
                for i in range(0, 5):
                    print('{:.3f} -> {}'.format(probs[i], classes[idx[i]]))

                # generate class activation mapping for the top1 prediction
                CAMs = returnCAM(features_blobs[0], weight_softmax, [idx[0]])

                # render the CAM and output
                print('| output CAM.jpg for the top1 prediction: %s' % answer)
                print("| Answer is %s" % ('Cardiomegaly'))
                img = cv2.imread('test.jpg')
                height, width, _ = img.shape
                heatmap = cv2.applyColorMap(cv2.resize(CAMs[0], (width, height)), cv2.COLORMAP_JET)
                result = heatmap * 0.3 + img * 0.5
                cv2.imwrite('CAM_%d_%d.jpg'%(epoch, batch_idx),result)
                net.train()
def heatmap(args):
    labels, imgs, label2idx, idx2label = load_train_data(args)
    traindata = TrainDataset(labels, imgs, args.img_dir)
    data_loader = DataLoader(traindata, batch_size=args.batch_size, shuffle=True, num_workers=4)

    #cam = CAM(data_loader, label2idx, idx2label, args)
    #cam.train_image_net()
    # networks such as googlenet, resnet, densenet already use global average pooling at the end, so CAM could be used directly.
    model_id = 2
    if model_id == 1:
        net = models.squeezenet1_1(pretrained=True)
        finalconv_name = 'features'  # this is the last conv layer of the network
    elif model_id == 2:
        net = models.resnet18(pretrained=True)
        finalconv_name = 'layer4'
    elif model_id == 3:
        net = models.densenet161(pretrained=True)
        finalconv_name = 'features'

    for param in net.parameters():
        param.requires_grad = False
        # Replace the last fully-connected layer
        # Parameters of newly constructed modules have requires_grad=True by default
    net.fc = nn.Linear(512, len(label2idx))
    print(net)

    #############eval to mack heatmap
    net.eval()
    # hook the feature extractor
    features_blobs = []

    def hook_feature(module, input, output):
        #print(output.data.cpu().numpy().shape)
        features_blobs.append(output.data.cpu().numpy())

    net._modules.get(finalconv_name).register_forward_hook(hook_feature)
    #print(net._modules.get(finalconv_name))
    # get the softmax weight
    params = list(net.parameters())
    weight_softmax = np.squeeze(params[-2].data.numpy())



    normalize = transforms.Normalize(
        mean=[0.485, 0.456, 0.406],
        std=[0.229, 0.224, 0.225]
    )
    preprocess = transforms.Compose([
        transforms.Scale((224, 224)),
        transforms.ToTensor(),
        normalize
    ])
    print("img id: ", imgs[0])
    #response = requests.get(IMG_URL)
    img_pil = Image.open('/tmp2/d04944017/ADLxMLDS2017/htc_project/images/'+imgs[0])

    img_tensor = preprocess(img_pil)
    img_variable = Variable(img_tensor.unsqueeze(0)).repeat(1, 3, 1, 1)
    #print(img_variable.size())
    #print(net)
    logit = net(img_variable)
    #print(len(logit[0]))

    # download the imagenet category list
    classes = idx2label#{int(key): value for (key, value) in requests.get(LABELS_URL).json().items()}
    print(classes)
    h_x = F.softmax(logit).data.squeeze()
    print("h_x", h_x.shape)
    probs, idx = h_x.sort(0, True)
    print(probs, idx)

    # output the prediction
    for i in range(0, 5):
        print('{:.3f} -> {}'.format(probs[i], classes[idx[i]]))

    # generate class activation mapping for the top1 prediction
    CAMs = returnCAM(features_blobs[0], weight_softmax, [idx[0]])

    # render the CAM and output
    print('output CAM.jpg for the top1 prediction: %s' % classes[idx[0]])
    img = cv2.imread('test.jpg')
    height, width, _ = img.shape
    heatmap = cv2.applyColorMap(cv2.resize(CAMs[0], (width, height)), cv2.COLORMAP_JET)
    result = heatmap * 0.3 + img * 0.5
    cv2.imwrite('CAM.jpg', result)
    ##########################
if __name__ == '__main__':
    if not os.path.exists('meta/'):
        os.makedirs('meta/')
    if not os.path.exists('model/'):
        os.makedirs('model/')
    if not os.path.exists('sample/'):
        os.makedirs('sample/')
    if not os.path.exists('output/'):
        os.makedirs('output/')
    args = parse()
    train(args)
    if args.test:
        pass