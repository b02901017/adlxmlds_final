# -*- coding: utf-8 -
NOFINDING = 'No Finding'
CLASS_NAMES = [ 'Atelectasis', 'Cardiomegaly', 'Effusion', 'Infiltrate', 'Mass', 'Nodule', 'Pneumonia',
                'Pneumothorax', 'Consolidation', 'Edema', 'Emphysema', 'Fibrosis', 'Pleural_Thickening', 'Hernia']

CLASS_WEIGHT = [0.1030949, 0.02475919, 0.11877453, 0.17743489, 0.05156975, 0.05646629,
                0.0127631, 0.04728862, 0.04162504, 0.02054049, 0.02244024, 0.01503746,
                0.0301908, 0.00202462]

label2label = {
    'Atelectasis': 'Atelectasis', 
    'Cardiomegaly': 'Cardiomegaly', 
    'Effusion': 'Effusion', 
    'Infiltration': 'Infiltration', 
    'Infiltrate': 'Infiltration',     
    'Mass': 'Mass', 
    'Nodule': 'Nodule', 
    'Pneumonia': 'Pneumonia', 
    'Pneumothorax': 'Pneumothorax', 
    'Consolidation': 'Consolidation', 
    'Edema': 'Edema', 
    'Emphysema': 'Emphysema', 
    'Fibrosis': 'Fibrosis', 
    'Pleural_Thickening': 'Pleural_Thickening', 
    'Hernia': 'Hernia'
}

label2size = {
	'Atelectasis' : [218,139,33508],
	'Cardiomegaly' : [479,381,184334],
	'Effusion' :[221,318,72002],
	'Infiltration' :[294,297,101381],
	'Mass' :[168,189,44378],
	'Nodule' :[71,70,5468],
	'Pneumonia' :[276,304,98815],
	'Pneumothorax' :[198,246,54644],
}

N_CLASSES = len(CLASS_NAMES)
MIN = 1e-8
MAX = 1e8
R_THRES = 0.2
B_THRES = 0.7