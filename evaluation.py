# -*- coding: utf-8 -
import numpy as np 
from sklearn.metrics import roc_auc_score
import cv2
from constants import *

def binarize(pred, thresh):
	pred = np.where(pred > thresh, pred, 0)
	pred = np.where(pred < thresh, pred, 1)        
	return pred

def compute_precision(gt, pred):
	PRECISIONS = []
	pred = binarize(pred, B_THRES)
	for i in range(N_CLASSES):
		t = gt[:, i]
		p = pred[:, i]
		t_ = t[np.where(p == 1)] 
		p_ = p[np.where(p == 1)]
		val = np.mean(p_ == t_)
		if not np.isnan(val):                                   
			PRECISIONS.append(val)
	precision = np.array(PRECISIONS).mean()
	if np.isnan(precision):
		return 0
	return precision

def compute_recall(gt, pred):
	RECALLs = []
	pred = binarize(pred, B_THRES)        
	for i in range(N_CLASSES):
		t = gt[:, i]
		p = pred[:, i]
		t_ = t[np.where(t == 1)] 
		p_ = p[np.where(t == 1)]
		val = np.mean(p_ == t_)
		if not np.isnan(val):                       
			RECALLs.append(val)
	recall = np.array(RECALLs).mean()
	if np.isnan(recall):
		return 0
	return recall

def compute_f1score(gt, pred):
	precision = compute_precision(gt, pred)
	recall = compute_recall(gt, pred)
	f1score = 2 / ( 1 / (precision + MIN) +  1 / (recall + MIN))
	return f1score

def compute_auroc(gt, pred):
	AUROCs = []
	for i in range(N_CLASSES):
		try :
			AUROCs.append(roc_auc_score(gt[:, i], pred[:, i]))
		except :
			pass
	return np.array(AUROCs).mean()

def compute_score(gt ,pred):
	_precision = compute_precision(gt, pred)
	_recall = compute_recall(gt, pred)
	_f1score = compute_f1score(gt, pred)
	_auroc = compute_auroc(gt, pred)
	return _precision, _recall, _f1score, _auroc

def compute_IoU(pred, gt):
	x_gt, y_gt, w_gt, h_gt = gt
	x_pred, y_pred, w_pred, h_pred = pred
	endx = max(x_gt + w_gt, x_pred + w_pred)
	startx = min(x_gt, x_pred)
	width = w_gt + w_pred - (endx - startx)
	endy = max(y_gt + h_gt, y_pred + h_pred)
	starty = min(y_gt, y_pred)
	height = h_gt + h_pred - (endy - starty)
	if width <= 0 or height <= 0:
		iou = 0.
	else:
		area = width * height
		area_gt = w_gt * h_gt
		area_pred = w_pred * h_pred
		iou = area * 1. / (area_gt + area_pred - area)
	return iou

