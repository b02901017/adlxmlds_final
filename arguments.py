# -*- coding: utf-8 -
import argparse
def parse():
    parser = argparse.ArgumentParser(description='ADL&MLDS final')
    parser.add_argument('--test', action='store_true', help='whether testing')
    parser.add_argument('--train', action='store_true', help='whether training')
    parser.add_argument('--draw', action='store_true', help='whether visulize the resilt')

    parser.add_argument('--load-model', action='store_true', help='whether load model')

    parser.add_argument('--img-dir', type=str, default='data/images/', help='img dir')
    parser.add_argument('--meta-dir', type=str, default='meta/', help='meta dir')    
    parser.add_argument('--model-dir', type=str, default='model/', help='model dir')    
    parser.add_argument('--sample-dir', type=str, default='sample/', help='sample dir')    
    parser.add_argument('--exp-dir', type=str, default='exp/', help='expirement dir')    

    parser.add_argument('--tag-path', type=str, default='data/Data_Entry_2017_v2.csv', help='tag file')        
    parser.add_argument('--box-path', type=str, default='data/BBox_List_2017.csv', help='box file')       
    parser.add_argument('--train-idx-path', type=str, default='data/train.txt', help='train idx')    
    parser.add_argument('--test-idx-path', type=str, default='data/test.txt', help='test idx')
    parser.add_argument('--valid-idx-path', type=str, default='data/valid.txt', help='valid idx')
    parser.add_argument('--imagenet-path', type=str, 
        default='model/imagenet_densenet121_10000.pkl', help='the best model path')    

    parser.add_argument('--pretrain-model', type=str, default='densenet121', help='pretrain model')    
    parser.add_argument('--shuffle', action='store_true', help='whether shuffle the data')
    parser.add_argument('--prepro', action='store_true', help='whether preprocessing')
    
    parser.add_argument('--use-cuda', action='store_true', help='whether use cuda')
    parser.add_argument('--use-pretrain', action='store_true', help='whether use pretrain model')
    
    parser.add_argument('--lr', type=float, default=1e-4, help="Learning rate of the classifier")
    parser.add_argument('--batch_size', type=int, default=32, help="Mini-batch size when training")
    parser.add_argument('--epochs', type=int, default = 20, help = 'total steps')
    parser.add_argument('--weight-flag', type=int, default = 1, help = 'which weight to use in bce loss')    
    parser.add_argument('--valid-ratio', type=float, default = 0.2, help = 'ratio of valid data')
    parser.add_argument('--attention-layer', type=int, default = 1, help = 'layer of attention map')

    parser.add_argument('--log-interval', type=int, default = 10, help = 'log frequency')
    parser.add_argument('--save-interval', type=int, default = 10000, help = 'save frequency')

    args = parser.parse_args()
    return args



def print_args(args):
    for k, v in vars(args).items():
        print('{:<16} : {}'.format(k, v))
